# combine-container

Standalone container for CMS Combine.

## Development

Determine used software versions:

```SHELL
scram tool info python3
gcc -v
scram tool info vdt
scram tool info pcre
scram tool info boost
scram tool info gsl
scram tool info xrootd
```

Mind that in particular the ROOT version might not exist in GitHub.
