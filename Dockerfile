ARG PLATFORM="linux/amd64"
FROM --platform=${PLATFORM} mambaorg/micromamba:latest

LABEL maintainer.name="Clemens Lange"
LABEL maintainer.email="clemens.lange@cern.ch"

# ENV LANG=C.UTF-8

ARG ROOT_VERSION=6.22
ARG PYTHON_VERSION=3.8
ARG COMBINE_TAG="v9.2.1"

WORKDIR /usr/local

RUN micromamba install -n base -y -c conda-forge python==${PYTHON_VERSION} pip pandas root==${ROOT_VERSION} gsl tbb vdt boost-cpp boost pcre eigen xrootd git make && \
    micromamba clean --all --yes

ARG MAMBA_DOCKERFILE_ACTIVATE=1

RUN git clone --depth 1 --single-branch https://github.com/novnc/noVNC.git ${HOME}/novnc

RUN echo "source ${HOME}/vnc_utils.sh" >> ${HOME}/.bashrc && \
    printf '\nulimit -s unlimited\n' >> ${HOME}/.bashrc && \
    printf '\nulimit -u unlimited > /dev/null 2>&1 || true\n' >> ${HOME}/.bashrc

ADD --chown=${MAMBA_USER}:${MAMBA_USER} vnc/vnc_utils.sh /home/${MAMBA_USER}/vnc_utils.sh
ENV GEOMETRY=1920x1080

RUN mkdir -p ${HOME}/.vnc
ADD --chown=${MAMBA_USER}:${MAMBA_USER} vnc/passwd ${HOME}/.vnc/passwd
ADD --chown=${MAMBA_USER}:${MAMBA_USER} .rootrc ${HOME}/.rootrc

WORKDIR /code

ENV CONDA=1

RUN git clone --depth 1 --single-branch \
        --branch ${COMBINE_TAG} \
        https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git \
        HiggsAnalysis/CombinedLimit \
    && cd HiggsAnalysis/CombinedLimit \
    && make -j$(nproc) \
    && mkdir -p /code/src \
    && cd /code/src \
    && ln -s ../HiggsAnalysis

ENV PYTHONPATH=$PYTHONPATH:/code/HiggsAnalysis/CombinedLimit/build/lib/python
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/code/HiggsAnalysis/CombinedLimit/build/lib:/opt/conda/lib/
ENV PATH=$PATH:/code/HiggsAnalysis/CombinedLimit/build/bin:/opt/conda/bin/
ENV CMSSW_BASE=/code

WORKDIR /code/HiggsAnalysis/CombinedLimit
